const Client = require("../models/client");
const { errorHandler } = require("../helpers/dbErrorHandler");

// middlewares rest 

exports.clientById = (req, res, next, id) => {
    Client.findById(id).exec((err, client) => {
        if (err || !client) {
            return res.status(404).json({
                error: "Category does not exist"
            });
        }
        req.client = client;
        next();
    });
};


exports.create = (req, res) => {
    const client = new Client(req.body);
    client.save((err, data) => {
        if (err) {
            return res.status(400).json({
                error: errorHandler(err)
            });
        }
        res.json( { data });
    });
};

exports.update = (req, res) => {
    const client = req.client;
    client.name = req.body.name;
    client.save((err, data) => {
        if (err) {
            return res.status(400).json({
                error: errorHandler(err)
            });
        }
        res.json(data);
    });
};

exports.remove = (req, res) => {
    const client = req.client;
    client.remove((err, data) => {
        if (err) {
            return res.status(400).json({
                error: errorHandler(err)
            });
        }
        res.json({
            message: "Client deleted"
        });
    });
};

exports.read = (req, res) => {
    return res.json(req.client);
};

exports.list = (req, res) => {
    Client.find().exec((err, data) => {
        if (err) {
            return res.status.json({
                error: errorHandler(err)
            });
        }
        res.json(data);
    });
};