const User = require("../models/user");
const { Quote } = require("../models/quotation");
const { errorHandler } = require("../helpers/dbErrorHandler");
const { read } = require("./client");

// middlewares rest 

exports.userById = (req, res, next, id) => {
    User.findById(id).exec((err, user) => {
        if (err || !user) {
            return res.status(400).json({
                error: "User not found"
            });
        }
        req.profile = user;
        next();
    });
};

exports.read = (req, res) => {
    req.profile.hashed_password = undefined;
    req.profile.salt = undefined;
    return res.json(req.profile);
};

exports.list = (req, res) => {
    User.find().exec((err, data) => {
        if (err) {
            return res.status.json({
                error: errorHandler(err)
            });
        }
        res.json(data);
    });
};

exports.update = (req, res) => {
    User.findOneAndUpdate(
        { _id: req.profile._id },
        { $set: req.body },
        { new: true },
        (err, user) => {
            if (err) {
                return res.status(400).json({
                    error: "You are not authorized to perform this action"
                });
            }
            user.hashed_password = undefined;
            user.salt = undefined;
            res.json(user);
        }
    );
};

exports.addQuoteToUserHistory = (req, res, next) => {
    let history = [];
    console.log(req.body);
    req.body.products.forEach(item => {
        history.push({
            _id: item.product,
            name: item.name,
            description: item.description,
            category: item.category,
            quantity: req.body.quantity,
            price: req.body.products.amount,
            discount: req.body.products.discount,
            
        });
        console.log(item.name);
    });

    User.findOneAndUpdate(
        { _id: req.profile._id },
        { $push: { history: history } },
        { new: true },
        (error, data) => {
            if (error) {
                return res.status(400).json({
                    error: "Could not update user purchase history"
                });
            }
            next();
        }
    );
};

exports.purchaseHistory = (req, res) => {
    Quote.find({ user: req.profile._id })
        .populate("user", "_id name")
        .sort("-created")
        .exec((err, quotes) => {
            if (err) {
                return res.status(400).json({
                    error: errorHandler(err)
                });
            }
            res.json(quotes);
        });
};
