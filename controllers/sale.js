const Sale = require("../models/sale");
const { errorHandler } = require("../helpers/dbErrorHandler");

// middlewares rest 

exports.saleById = (req, res, next, id) => {
  
    Sale.findById(id).exec((err, quote) => {
      //  console.log(quote);
            if (err || !quote) {
                return res.status(400).json({
                    error: "Sale not found"
                });
            }
            req.profile = quote;
            next();
        });
};


exports.create = (req, res) => {

    //console.log(req.body);
    //req.body.user = req.profile;
    
    const sale = new Sale(req.body);
    sale.save((error, data) => {
        if (error) {
            return res.status(400).json({
                error: errorHandler(error)
            });
        }
        res.json(data);
    });
};

exports.listSales = (req, res) => {
    Sale.find()
        .populate("user", "_id name email")
        .populate("client", "_id name address rut")
        .sort("-created")
        .exec((err, data) => {
            if (err) {
                return res.status(400).json({
                    error: errorHandler(err)
                });
            }
            res.json(data);
        });
};

exports.listSalesById = (req, res) => {
    Sale.find( { _id: req.params.saleId })
        .populate("user", "_id name email")
        .populate("client", "_id name address rut")
        .sort("-created")
        .exec((err, data) => {
            if (err) {
                return res.status(400).json({
                    error: errorHandler(err)
                });
            }
            res.json(data);
        });
};


exports.remove = (req, res) => {
   // console.log(req.body);
   Sale.findOneAndRemove(
        { _id: req.params.saleId },
            (err, data) => {
            if (err) {
                return res.status(400).json({
                    error: "You are not authorized to perform this action"
                });
            }
            
            res.json(data);
        }
    );
};


