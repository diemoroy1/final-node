const QuoteDetail = require("../models/quoteDetail");
const { errorHandler } = require("../helpers/dbErrorHandler");



exports.create = (req, res) => {

    //console.log(req.body);
    //req.body.user = req.profile;
    
    const quote = new QuoteDetail(req.body);
    quote.save((error, data) => {
        if (error) {
            return res.status(400).json({
                error: errorHandler(error)
            });
        }
        res.json(data);
    });
};

exports.listDetailQuotation = (req, res) => {
    QuoteDetail.find()
        .populate("quote", "_id" )
        .populate("product", "_id name description")
        .exec((err, data) => {
            if (err) {
                return res.status(400).json({
                    error: errorHandler(err)
                });
            }
            res.json(data);
        });
};

exports.listQuotationId = (req, res) => {
    QuoteDetail.find({ _id: req.params.quoteDetailId })
        .populate("quote", "_id" )
        .populate("product", "_id name description")
        .exec((err, data) => {
            if (err) {
                return res.status(400).json({
                    error: errorHandler(err)
                });
            }
            res.json(data);
        });
};


exports.update = (req, res) => {
   // console.log(req.body);
   QuoteDetail.findOneAndUpdate(
        { _id: req.params.quoteDetailId },
        { $set: req.body },
        { new: true },
        (err, data) => {
            if (err) {
                return res.status(400).json({
                    error: "You are not authorized to perform this action"
                });
            }
            
            res.json(data);
        }
    );
};


exports.deleteDetail = (req, res) => {
    let quote = req.quoteDetail;
    quote.remove((err, deletedQuote) => {
        if (err) {
            return res.status(400).json({
                error: errorHandler(err)
            });
        }
        res.json({
            message: "Product deleted successfully"
        });
    });
};

