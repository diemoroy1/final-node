const Quote = require("../models/quotation");
const { errorHandler } = require("../helpers/dbErrorHandler");

// middlewares rest 

exports.quoteById = (req, res, next, id) => {
  
    Quote.findById(id).exec((err, quote) => {
      //  console.log(quote);
            if (err || !quote) {
                return res.status(400).json({
                    error: "Quote not found"
                });
            }
            req.profile = quote;
            next();
        });
};


exports.create = (req, res) => {

    //console.log(req.body);
    //req.body.user = req.profile;
    
    const quote = new Quote(req.body);
    quote.save((error, data) => {
        if (error) {
            return res.status(400).json({
                error: errorHandler(error)
            });
        }
        res.json(data);
    });
};

exports.listQuotations = (req, res) => {
    Quote.find()
        .populate("user", "_id name email")
        .populate("client", "_id name address rut")
        .sort("-created")
        .exec((err, data) => {
            if (err) {
                return res.status(400).json({
                    error: errorHandler(err)
                });
            }
            res.json(data);
        });
};

exports.getStatusValues = (req, res) => {
    res.json(Quote.schema.path("status").enumValues);
};

exports.updateQuoteStatus = (req, res) => {

    Quote.update(
        { _id: req.params.quoteId  },
        { $set: { status: req.body.status } },
        (err, data) => {
            if (err) {
                return res.status(400).json({
                    error: errorHandler(err)
                });
            }
            res.json(data);
        }
    );
};


exports.update = (req, res) => {
   // console.log(req.body);
    Quote.findOneAndUpdate(
        { _id: req.params.quoteId },
        { $set: req.body },
        { new: true },
        (err, data) => {
            if (err) {
                return res.status(400).json({
                    error: "You are not authorized to perform this action"
                });
            }
            
            res.json(data);
        }
    );
};
