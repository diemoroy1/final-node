const SaleLog = require("../models/saleLog");
const { errorHandler } = require("../helpers/dbErrorHandler");

// middlewares rest 

exports.saleLogById = (req, res, next, id) => {
  
    SaleLog.findById(id).exec((err, quote) => {
      //  console.log(quote);
            if (err || !quote) {
                return res.status(400).json({
                    error: "Sale not found"
                });
            }
            req.profile = quote;
            next();
        });
};


exports.create = (req, res) => {

    //console.log(req.body);
    //req.body.user = req.profile;
    
    const saleLog = new SaleLog(req.body);
    saleLog.save((error, data) => {
        if (error) {
            return res.status(400).json({
                error: errorHandler(error)
            });
        }
        res.json(data);
    });
};

exports.listSaleLog = (req, res) => {
    SaleLog.find()
        .populate("sale", "_id amount")
        .populate("product", "_id name ")
        .exec((err, data) => {
            if (err) {
                return res.status(400).json({
                    error: errorHandler(err)
                });
            }
            res.json(data);
        });
};

exports.listSaleLogBySaleId = (req, res) => {
    SaleLog.find( { _id: req.params.saleSlogId })
        .populate("sale", "_id amount")
        .populate("product", "_id name")
        .sort("-created")
        .exec((err, data) => {
            if (err) {
                return res.status(400).json({
                    error: errorHandler(err)
                });
            }
            res.json(data);
        });
};


exports.remove = (req, res) => {
   // console.log(req.body);
   SaleLog.findOneAndRemove(
        { _id: req.params.saleId },
            (err, data) => {
            if (err) {
                return res.status(400).json({
                    error: "You are not authorized to perform this action"
                });
            }
            
            res.json(data);
        }
    );
};





exports.decreaseQuantity = (req, res, next) => {
    let bulkOps = req.body.products.map(item => {
        console.log("item_ID: "+item.id);
        return {
            updateOne: {
                
                filter: { _id: item._id },
                update: { $inc: { quantity: -item.count, sold: +item.count } }
            }
        };
    });

    Product.bulkWrite(bulkOps, {}, (error, products) => {
        if (error) {
            return res.status(400).json({
                error: "Could not update product"
            });
        }
        next();
    });
};
