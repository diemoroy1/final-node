const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;


const quoteDetailSchema = new mongoose.Schema(
    {
        quote: {type: ObjectId, ref: "Quote" },
        product: {type: ObjectId, ref: "Product" },
        price: {
            type: Number,
            trim: true,
            required: true,
            maxlength: 32
        },
        quantity: {
            type: Number,
            trim: true,
            required: true,
            maxlength: 32
        }
    },
    
);


module.exports = mongoose.model("quoteDetail", quoteDetailSchema);