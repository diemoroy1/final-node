const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const saleSchema = new mongoose.Schema(
    {
        quote: {type: ObjectId, ref: "Quote" },
        amount: {
            type: Number,
            trim: true,
            required: true,
            maxlength: 32
        },
        sold: Date,
    },
    
);


module.exports = mongoose.model("Sale", saleSchema);