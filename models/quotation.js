const mongoose = require("mongoose");
// const Schema = mongoose.Schema;
const { ObjectId } = mongoose.Schema;

const QuoteSchema = new mongoose.Schema(
  {
    amount: { type: Number },
    status: {
      type: String,
      default: "Started",
      enum: ["Started", "Processing", "Answered", "Acepted", "Cancelled"] // enum means string objects
    },
    user: { type: ObjectId, ref: "User" },
    client: { type: ObjectId, ref: "Client" },
    updated: Date,
  },
  { timestamps: true }
);

module.exports = mongoose.model("Quote", QuoteSchema);
