const mongoose = require("mongoose");

const clientSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            trim: true,
            required: true,
            maxlength: 32,
            unique: true
        },
        address: {
            type: String,
            trim: true,
            required: true,
            maxlength: 80,
        },
        rut: {
            type: String,
            trim: true,
            required: true,
            maxlength: 10,
            unique: true
        },
    },
    { timestamps: true }
);


module.exports = mongoose.model("Client", clientSchema);


  