const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const salesLogSchema = new mongoose.Schema(
    {
        sale: { type: ObjectId, ref: "Sale" },
        product: {type: ObjectId, ref: "Product" },
        price: {
            type: Number,
            trim: true,
            required: true,
            maxlength: 32
        },
        move:{
            type: String,
            default: "In",
            enum: ["In", "Out"] 
        },
        soldDate: Date,
    },
    
);


module.exports = mongoose.model("SalesLog", salesLogSchema);