api-Quotation-node

Proyecto realizado con NODE.JS para app teameet

Quotation
Permite generar una cotización en linea,  permitiendo a los usuarios modificar los datos de la cotización y responderla al cliente.
Este al aceptar la cotización, finalmente se hace la venta

Pre-requisitos 
Es necesario instalar Node, MongoDB

Levantar el proyecto 
Instalar dependencias del proyecto con npm install, configurar archivo .env luego levantar el proyecto con npm run start

local -> http://localhost:8000

Documentación api


Swagger -> http://localhost:8000/api-docs/

Postman -> https://www.getpostman.com/collections/4ff7c849fd7e54bd68c2

Construido con 
NodeJS
Express
MongoDB
