const express = require("express");
const router = express.Router();

const {
    create,
    productById,
    read,
    remove,
    update,
    list,
    listRelated,
    listCategories,
    listBySearch,
    photo,
    listSearch
} = require("../controllers/product");
const { requireSignin, isAuth, isAdmin } = require("../controllers/auth");
const { userById } = require("../controllers/user");

// routes
/**
 * @swagger
 * /api/product/{productId}:
 *  get:
 *    summary: Product by Id
 *    description: Use to get Products by Id
 *    tags: 
 *          - Products 
 *    parameters:
 *      - name: productId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/product/:productId", read);

 /**
 * @swagger   
 * /api/product/create/{userId}: 
 *  post:
 *    summary: Creste Product
 *    description: Create Product
 *    tags:  
 *          - Products 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: string
 *        required: true
 * 
 *
 *    requestBody: 
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            type: object
 *            properties:
 *              name:
 *                  type: string
 *                  description: name user
 *              description:
 *                  type: string
 *                  description: description must be good
 *              price:
 *                  type: number
 *                  description: price must be a number
 *              category:
 *                  type: string
 *                  description: Must be a category valid
 *              quantity:
 *                  type: number
 *                  description: Quantity must be a number
 *              uniMed:
 *                  type: string
 *                  description: mesure Unit must be (MTS,KGS, UND, PLS, TRS)
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 */
router.post("/product/create/:userId", requireSignin, isAuth, isAdmin, create);


 /**
 * @swagger   
 * /api/product/{productId}/{userId}: 
 *  delete:
 *    summary: Delete product by id
 *    description: Delete product by id
 *    tags:  
 *          - Products 
 *    parameters:
 *      - name: productId
 *        in: path
 *        type: integer
 *        required: true
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 *
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 */
router.delete(
    "/product/:productId/:userId",
    requireSignin,
    isAuth,
    isAdmin,
    remove
);

/**
 * @swagger   
 * /api/product/{productId}/{userId}: 
 *  put:
 *    summary: Update Product
 *    description: Update Product
 *    tags:  
 *          - Products 
 *    parameters:
 *      - name: productId
 *        in: path
 *        type: integer
 *        required: true
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *
 *    requestBody: 
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            type: object
 *            properties:
 *              name:
 *                  type: string
 *                  description: name user
 *              description:
 *                  type: string
 *                  description: description must be good
 *              price:
 *                  type: number
 *                  description: price must be a number
 *              category:
 *                  type: string
 *                  description: Must be a category valid
 *              quantity:
 *                  type: number
 *                  description: Quantity must be a number
 *              uniMed:
 *                  type: string
 *                  description: mesure Unit must be (MTS,KGS, UND, PLS, TRS)
 *             
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 */
router.put(
    "/product/:productId/:userId",
    requireSignin,
    isAuth,
    isAdmin,
    update
);

/**
 * @swagger
 * /api/products:
 *  get:
 *    summary: Product List
 *    description: Use to get Product List
 *    tags:  
 *          - Products 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/products", list);

/**
 * @swagger
 * /api/products/search:
 *  get:
 *    summary: Product List
 *    description: Use to get Product List
 *    tags:  
 *          - Products 
 *    
 *    parameters:
 *      - name: name
 *        in: query
 *        type: string
 *        required: true
 *      - name: category
 *        in: query
 *        type: integer
 *        required: false
 * 
 *
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/products/search", listSearch);

/**
 * @swagger
 * /api/products/related/{productId}:
 *  get:
 *    summary: Product Related by Id
 *    description: Use to get Products Related by Id
 *    tags: 
 *          - Products 
 *    parameters:
 *      - name: productId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/products/related/:productId", listRelated);

/**
 * @swagger
 * /api/products/categories:
 *  get:
 *    summary: Product Related by Id
 *    description: Use to get Products Related by Id
 *    tags: 
 *          - Products 
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/products/categories", listCategories);

/**
 * @swagger   
 * /api/products/by/search: 
 *  post:
 *    summary: Update Product
 *    description: Update Product
 *    tags:  
 *          - Products 
 *
 *    requestBody: 
 *      content:
 *        application/json:
 *          schema:
 *            properties:
 *              order:
 *                  type: string
 *                  description: order by
 *              sortBy:
 *                  type: string
 *                  description: sortBy
 *              limit:
 *                  type: number
 *                  description: limit
 *              skip:
 *                  type: string
 *                  description: skip
 *              
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 */
router.post("/products/by/search", listBySearch);
//router.get("/product/photo/:productId", photo);

// params
router.param("userId", userById);
router.param("productId", productById);

module.exports = router;