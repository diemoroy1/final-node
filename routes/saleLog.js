const express = require("express");
const router = express.Router();

const { requireSignin, isAuth, isAdmin } = require("../controllers/auth");
const { userById} = require("../controllers/user");
const { saleById } = require("../controllers/sale");
const {
    create,
    saleLogById,
    remove,
    listSaleLog,
    listSaleLogBySaleId,
    update
} = require("../controllers/saleLog");


// routes

/**
 * @swagger   
 * /api/saleLog/create/{userId}: 
 *  post:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Sale Log
 *    summary: Create a Sale
 *    description: Use to request create Sale
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    requestBody: 
 *      content:
 *        application/json:
 *          schema:
 *            properties:
 *              sale:
 *                  type: string
 *                  description: id Sale
 *              product:
 *                  type: string
 *                  description: is Product
 *              price:
 *                  type: integer
 *                  description: price of sale
 *              move:
 *                  type: string
 *                  description: type of movement              
 *               
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 */
router.post(
    "/saleLog/create/:userId",
    requireSignin,
    isAuth,
    create
);

/**
 * @swagger
 * /api/saleLog/list/{userId}:
 *  get:
 *    summary: Product Related by Id
 *    description: Use to get Products Related by Id
 *    tags: 
 *          - Sale Log 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/saleLog/list/:userId", requireSignin, isAuth, isAdmin, listSaleLog);

/**
 * @swagger
 * /api/sale/{userId}/{saleId}:
 *  get:
 *    summary: Sales related by Id
 *    description: Use to get SAles Related by Id
 *    tags: 
 *          - Sale Log 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 *      - name: saleId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get(
    "/saleLog/:userId/:saleId",
    requireSignin,
    isAuth,
    listSaleLogBySaleId
);


/**
 * @swagger   
 * /api/saleLog/{saleLogId}/{userId}: 
 *  update:
 *    security:
 *      - bearerAuth: []
 *    summary: Delete Sale
 *    description: Delete Sale
 *    tags:
 *        - Sale Log 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 *      - name: saleLogId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *             
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 * 
 */
router.put("/saleLog/:saleLogId/:userId", requireSignin, isAuth, isAdmin, update);

/**
 * @swagger   
 * /api/saleLog/{saleLogId}/{userId}: 
 *  delete:
 *    security:
 *      - bearerAuth: []
 *    summary: Delete Sale
 *    description: Delete Sale
 *    tags:
 *        - Sale Log 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 *      - name: saleLogId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *             
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 * 
 */
router.delete("/saleLog/:saleLogId/:userId", requireSignin, isAuth, isAdmin, remove);

// params
router.param("userId", userById);
router.param("saleLogId", saleLogById);
router.param("saleId", saleById);


module.exports = router;
