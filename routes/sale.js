const express = require("express");
const router = express.Router();

const { requireSignin, isAuth, isAdmin } = require("../controllers/auth");
const { userById} = require("../controllers/user");
const { quoteById } = require("../controllers/quotation");
const {
    create,
    listSales,
    saleById,
    remove,
    listSalesById
} = require("../controllers/sale");


// routes

/**
 * @swagger   
 * /api/sale/create/{userId}: 
 *  post:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Sale
 *    summary: Create a Sale
 *    description: Use to request create Sale
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    requestBody: 
 *      content:
 *        application/json:
 *          schema:
 *            properties:
 *              quote:
 *                  type: string
 *                  description: Quotation
 *              amount:
 *                  type: integer
 *                  description: amount of sole
 *              sold:
 *                  type: string
 *                  description: date o sale
 *               
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 */
router.post(
    "/sale/create/:userId",
    requireSignin,
    isAuth,
    create
);

/**
 * @swagger
 * /api/sale/list/{userId}:
 *  get:
 *    summary: Product Related by Id
 *    description: Use to get Products Related by Id
 *    tags: 
 *          - Sale 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/sale/list/:userId", requireSignin, isAuth, isAdmin, listSales);

/**
 * @swagger
 * /api/sale/{userId}/{saleId}:
 *  get:
 *    summary: Sales related by Id
 *    description: Use to get SAles Related by Id
 *    tags: 
 *          - Sale 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 *      - name: saleId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get(
    "/sale/:userId/:saleId",
    requireSignin,
    isAuth,
    listSalesById
);


/**
 * @swagger   
 * /api/sale/{quoteId}/{userId}: 
 *  delete:
 *    security:
 *      - bearerAuth: []
 *    summary: Delete Sale
 *    description: Delete Sale
 *    tags:
 *        - Sale 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 *      - name: quoteId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *             
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 * 
 */
router.delete("/sale/:saleId/:userId", requireSignin, isAuth, isAdmin, remove);

// params
router.param("userId", userById);
router.param("quoteId", quoteById);
router.param("saleId", saleById);


module.exports = router;
