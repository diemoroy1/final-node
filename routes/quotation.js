const express = require("express");
const router = express.Router();

const { requireSignin, isAuth, isAdmin } = require("../controllers/auth");
const { userById} = require("../controllers/user");
const {
    create,
    listQuotations,
    getStatusValues,
    quoteById,
    updateQuoteStatus,
    update
} = require("../controllers/quotation");


// routes

/**
 * @swagger   
 * /api/quotation/create/{userId}: 
 *  post:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Quotation
 *    summary: Create a Quotation
 *    description: Use to request create quote
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    requestBody: 
 *      content:
 *        application/json:
 *          schema:
 *            properties:
 *              amount:
 *                  type: integer
 *                  description: Amount of quotation
 *              status:
 *                  type: string
 *                  description: Status of quotation
 *              user:
 *                  type: string
 *                  description: valid userid
 *              client:
 *                  type: string
 *                  description: valid clientid
 *              updated:
 *                  type: string
 *                  description: date of change
 *                 
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 */
router.post(
    "/quotation/create/:userId",
    requireSignin,
    isAuth,
    create
);

/**
 * @swagger
 * /api/quotation/list/{userId}:
 *  get:
 *    summary: Product Related by Id
 *    description: Use to get Products Related by Id
 *    tags: 
 *          - Quotation 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/quotation/list/:userId", requireSignin, isAuth, isAdmin, listQuotations);

/**
 * @swagger
 * /api/quotation/status-values/{userId}:
 *  get:
 *    summary: Product Related by Id
 *    description: Use to get Products Related by Id
 *    tags: 
 *          - Quotation 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get(
    "/quotation/status-values/:userId",
    requireSignin,
    isAuth,
    isAdmin,
    getStatusValues
);

/**
 * @swagger   
 * /api/quotation/{quoteId}/status/{userId}: 
 *  put:
 *    security:
 *      - bearerAuth: []
 *    summary: Modify Quotation
 *    description: Modify Quotation
 *    tags:
 *        - Quotation 
 *    parameters:
 *      - name: quoteId
 *        in: path
 *        type: integer
 *        required: true
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *
 *    requestBody: 
 *      content:
 *        application/json:
 *          schema:
 *            properties:
 *              status:
 *                  type: string
 *                  description: Status of quotation
 *             
 *             
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 *      "401":
 *         description: Access token is missing or invalid
 */
router.put(
    "/quotation/:quoteId/status/:userId",
    requireSignin,
    isAuth,
    isAdmin,
    updateQuoteStatus
);


/**
 * @swagger   
 * /api/quotation/{quoteId}/{userId}: 
 *  put:
 *    security:
 *      - bearerAuth: []
 *    summary: Modify Quotation
 *    description: Modify Quotation
 *    tags:
 *        - Quotation 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 *      - name: quoteId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *
 *    requestBody: 
 *      content:
 *        application/json:
 *          schema:
 *            properties:
 *               amount:
 *                  type: integer
 *                  description: Amount of quotation
 *               user:
 *                  type: string
 *                  description: valid userid
 *               client:
 *                  type: string
 *                  description: valid clientid
 *               updated:
 *                  type: string
 *                  description: date of change
 *             
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 * 
 */
router.put("/quotation/:quoteId/:userId", requireSignin, isAuth, update);

// params
router.param("userId", userById);
router.param("quoteId", quoteById);

module.exports = router;
