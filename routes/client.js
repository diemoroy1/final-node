const express = require("express");
const router = express.Router();

const {
    clientById,
    create,
    update,
    remove,
    read,
    list
} = require("../controllers/client");
const { requireSignin, isAuth, isAdmin } = require("../controllers/auth");
const { userById } = require("../controllers/user");


// routes
/**
 * @swagger
 * /api/client/{clientById}:
 *  get:
 *    summary: Client by Id
 *    description: Use to get Client Info
 *    tags: 
 *          - Client 
 *    parameters:
 *      - name: clientById
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/client/:clientById", read);

/**
 * @swagger   
 * /api/client/create/{userId}: 
 *  post:
 *    security:
 *      - bearerAuth: []
 *    summary: Create client
 *    description: Create client
 *    tags:
 *        - Client 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *
 *    requestBody: 
 *      content:
 *        application/json:
 *          schema:
 *            properties:
 *              name:
 *                  type: string
 *                  description: name category
 *              address:
 *                  type: string
 *                  description: name category
 *              rut:
 *                  type: string
 *                  description: name category
 *             
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 *      "401":
 *         description: Access token is missing or invalid
 */
router.post("/client/create/:userId", requireSignin, isAuth, isAdmin, create);

 /**
 * @swagger   
 * /api/client/{clientById}/{userId}: 
 *  put:
 *    security:
 *      - bearerAuth: []
 *    summary: Modify Client
 *    description: Modify Client
 *    tags:
 *        - Client
 *    parameters:
 *      - name: clientById
 *        in: path
 *        type: integer
 *        required: true
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *
 *    requestBody: 
 *      content:
 *        application/json:
 *          schema:
 *            properties:
 *              name:
 *                  type: string
 *                  description: name category
 *              address:
 *                  type: string
 *                  description: name category
 *              rut:
 *                  type: string
 *                  description: name category
 *             
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 *      "401":
 *         description: Access token is missing or invalid
 */
router.put(
    "/client/:clientById/:userId",
    requireSignin,
    isAuth,
    isAdmin,
    update
);

/**
 * @swagger   
 * /api/client/{clientById}/{userId}: 
 *  delete:
 *    security:
 *      - bearerAuth: []
 *    summary: Modify category
 *    description: Modify category
 *    tags:
 *        - Client 
 *    parameters:
 *      - name: clientById
 *        in: path
 *        type: integer
 *        required: true
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 *      "401":
 *         description: Access token is missing or invalid
 */
router.delete(
    "/client/:clientById/:userId",
    requireSignin,
    isAuth,
    isAdmin,
    remove
);

/**
 * @swagger
 * /api/clients:
 *  get:
 *    summary: Client List
 *    description: Use to get Client List
 *    tags: 
 *          - Client 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/clients", list);


// params
router.param("clientById", clientById);
router.param("userId", userById);


module.exports = router;

