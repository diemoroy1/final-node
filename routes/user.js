const express = require("express");
const router = express.Router();

const { requireSignin, isAuth, isAdmin } = require("../controllers/auth");

const {
    userById,
    list,
    read,
    update,
    purchaseHistory
} = require("../controllers/user");

// routes
/**
 * @swagger
 * /api/secret/{userId}:
 *  get:
 *    summary: Secret Info by User Id
 *    description: Use to get secret Info
 *    tags: 
 *          - Users 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/secret/:userId", requireSignin, isAuth, isAdmin, (req, res) => {
    res.json({
        user: req.profile
    });
});

/**
 * @swagger
 * /api/user/{userId}:
 *  get:
 *    summary: User by Id
 *    description: Use to get User Info
 *    tags: 
 *          - Users 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/user/:userId", requireSignin, isAuth, read);


/**
 * @swagger
 * /api/user:
 *  get:
 *    summary: List of Users 
 *    description: Use to get list of Users
 *    tags: 
 *          - Users 
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/user", requireSignin, list);


/**
 * @swagger   
 * /api/user/{userId}: 
 *  put:
 *    security:
 *      - bearerAuth: []
 *    summary: Modify User
 *    description: Modify User
 *    tags:
 *        - Users 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *
 *    requestBody: 
 *      content:
 *        application/json:
 *          schema:
 *            properties:
 *              name:
 *                  type: string
 *                  description: name category
 *              email:
 *                  type: string
 *                  description: email user valid
 *              password:
 *                  type: string
 *                  description: password user valid
 *              about:
 *                  type: string
 *                  description: password user valid
 *             
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 *      "401":
 *         description: Access token is missing or invalid
 */
router.put("/user/:userId", requireSignin, isAuth, update);

/**
 * @swagger
 * /api/quotes/by/user/{userId}:
 *  get:
 *    summary: Quotes by User
 *    description: Use to get quotes by user
 *    tags: 
 *          - Users 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/quotes/by/user/:userId", requireSignin, isAuth, purchaseHistory);

// params
router.param("userId", userById);

module.exports = router;