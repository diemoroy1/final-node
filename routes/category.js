const express = require("express");
const router = express.Router();

const {
    create,
    categoryById,
    read,
    update,
    remove,
    list
} = require("../controllers/category");
const { requireSignin, isAuth, isAdmin } = require("../controllers/auth");
const { userById } = require("../controllers/user");

// routes
/**
 * @swagger
 * /api/category/{categoryId}:
 *  get:
 *    summary: Category List
 *    description: Use to get Category List
 *    tags: 
 *          - Categories 
 *    parameters:
 *      - name: categoryId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/category/:categoryId", read);


 /**
 * @swagger   
 * /api/category/create/{userId}: 
 *  post:
 *    security:
 *      - bearerAuth: []
 *    summary: Create category
 *    description: Create category
 *    tags:
 *        - Categories 
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *
 *    requestBody: 
 *      content:
 *        application/json:
 *          schema:
 *            properties:
 *              name:
 *                  type: string
 *                  description: name category
 *             
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 *      "401":
 *         description: Access token is missing or invalid
 */
router.post("/category/create/:userId", requireSignin, isAuth, isAdmin, create);

 /**
 * @swagger   
 * /api/category/{categoryId}/{userId}: 
 *  put:
 *    security:
 *      - bearerAuth: []
 *    summary: Modify category
 *    description: Modify category
 *    tags:
 *        - Categories 
 *    parameters:
 *      - name: categoryId
 *        in: path
 *        type: integer
 *        required: true
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *
 *    requestBody: 
 *      content:
 *        application/json:
 *          schema:
 *            properties:
 *              name:
 *                  type: string
 *                  description: name category
 *             
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 *      "401":
 *         description: Access token is missing or invalid
 */
router.put(
    "/category/:categoryId/:userId",
    requireSignin,
    isAuth,
    isAdmin,
    update
);


 /**
 * @swagger   
 * /api/category/{categoryId}/{userId}: 
 *  delete:
 *    security:
 *      - bearerAuth: []
 *    summary: Modify category
 *    description: Modify category
 *    tags:
 *        - Categories 
 *    parameters:
 *      - name: categoryId
 *        in: path
 *        type: integer
 *        required: true
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 *      "401":
 *         description: Access token is missing or invalid
 */
router.delete(
    "/category/:categoryId/:userId",
    requireSignin,
    isAuth,
    isAdmin,
    remove
);

/**
 * @swagger
 * /api/categories:
 *  get:
 *    summary: Category List
 *    description: Use to get Category List
 *    tags: 
 *          - Categories 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/categories", list);

// params
router.param("categoryId", categoryById);
router.param("userId", userById);

module.exports = router;

