const express = require("express");
const router = express.Router();

const { requireSignin, isAuth, isAdmin } = require("../controllers/auth");
const { userById } = require("../controllers/user");
const { quoteById } = require("../controllers/quotation");
const { create, listDetailQuotation, update, deleteDetail, listQuotationId } = require("../controllers/quoteDetail");


// routes

/**
 * @swagger   
 * /api/quotation/detail/create/{userId}: 
 *  post:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Quotation Detail
 *    summary: Create a Quotation
 *    description: Use to request create quote
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    requestBody: 
 *      content:
 *        application/json:
 *          schema:
 *            properties:
 *              quote:
 *                  type: string
 *                  description: id of quotation
 *              product:
 *                  type: string
 *                  description: product of quotation
 *              price:
 *                  type: integer
 *                  description: price of product
 *              quantity:
 *                  type: integer
 *                  description: price of product
 *             
 *                 
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 */
router.post(
    "/quotation/detail/create/:userId",
    requireSignin,
    isAuth,
    create
);

/**
 * @swagger
 * /api/quotation/detail/list/{userId}:
 *  get:
 *    summary: Product Related by Id
 *    description: Use to get Products Related by Id
 *    tags: 
 *          - Quotation Detail
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/quotation/detail/list/:userId", requireSignin, isAuth, listDetailQuotation);

/**
 * @swagger
 * /api/quotation/detail/{quoteDetailId}/{userId}:
 *  get:
 *    summary: Product Related by Id
 *    description: Use to get Products Related by Id
 *    tags: 
 *          - Quotation Detail
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 *      - name: quoteDetailId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *    responses:
 *      "200":
 *        description: A successful response
 */
router.get("/quotation/detail/:quoteDetailId/:userId", requireSignin, isAuth, listQuotationId);



/**
 * @swagger   
 * /api/quotation/detail/{quoteDetailId}/{userId}: 
 *  delete:
 *    security:
 *      - bearerAuth: []
 *    summary: Modify Quotation
 *    description: Modify Quotation
 *    tags:
 *        - Quotation Detail
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 *      - name: quoteDetailId
 *        in: path
 *        type: integer
 *        required: true
 * 
 * 
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 * 
 */
router.delete("/quotation/detail/:quoteDetailId/:userId", requireSignin, isAuth, deleteDetail);

/**
 * @swagger   
 * /api/quotation/detail/{quoteDetailId}/{userId}: 
 *  put:
 *    security:
 *      - bearerAuth: []
 *    summary: Modify Quotation
 *    description: Modify Quotation
 *    tags:
 *        - Quotation Detail
 *    parameters:
 *      - name: userId
 *        in: path
 *        type: integer
 *        required: true
 *      - name: quoteDetailId
 *        in: path
 *        type: integer
 *        required: true
 * 
 *
 *    requestBody: 
 *      content:
 *        application/json:
 *          schema:
 *            properties:
 *              product:
 *                  type: string
 *                  description: product of quotation
 *              price:
 *                  type: integer
 *                  description: price of product
 *              quantity:
 *                  type: integer
 *                  description: price of product
 *             
 *    responses:
 *      "200":
 *         description: A successful response
 *      "400":
 *         description: A bad request response
 * 
 */
router.put("/quotation/detail/:quoteDetailId/:userId", requireSignin, isAuth, update);

// params
router.param("userId", userById);
router.param("quoteId", quoteById);

module.exports = router;
